import math
trans = (math.pi*2)/360
for i in range(0,361,1):
    print("-----------",i,"度----------")
    print("sin("+str(i)+")=",math.sin(i*trans))
    print("cos("+str(i)+")=",math.cos(i*trans))